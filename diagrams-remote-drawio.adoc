:toc:
:toc-title:
:sectnums:
:toclevels: 3

= Draw.io in AsciiDoc

== Overview

link:https://www.draw.io/[Draw.io] is an online diagram editor for making flowcharts, process diagrams, org charts, UML, ER and network diagrams.

In contrast to PlantUML or Mermaid, Draw.io diagrams are not created from a plain plain text language. Draw.io diagrams are created with a graphical online editor.

== GitLab

As a consecuence Draw.io diagrams cannot be defined in AsciiDoc and in GitLab directly.

However, Draw.io supports GitLab as storage backend. This enables Draw.io to create new diagrams in a GitLab repository and also to update existing diagrams.

=== Create a new Draw.io Diagram

New diagrams can be created with the link:https://www.draw.io/?mode=gitlab[Draw.io online editor]. They can be saved in a GitLab repository of a linked account.

Draw.io supports multiple storage formats. The default is the `.drawio` format, which is based on XML. However, this format should not be used, beceause AsciiDoc is not able to render such diagrams.

Fortunately, Draw.io supports an extended `png` and `svg` storage format, which can be displayed like noraml `png` and `svg` files, but also contains additional information that is required by Draw.io to edit them.

=== Link an existing Draw.io Diagram

A diagram created by Draw.io and which has been stored as `png` or `svg` file can be included in a AsciiDoc document like any other image:

[source,asciidoc]
----
image::drawio-diagram.png[Diagram]
----

image::drawio-diagram.png[Diagram]

link:https://www.draw.io/?mode=gitlab#Aueisele-playground%2Fdoc%2Fmaster%2Fdrawio-diagram.png[Edit] | link:https://www.draw.io/#Uhttps%3A%2F%2Fgitlab.com%2Fueisele-playground%2Fdoc%2F-%2Fraw%2Fmaster%2Fdrawio-diagram.png[Edit As New]

Because the `png` file also contains extended information, Draw.io can also edit this image file.

The following AsciiDoc snippet creates a link to open link:drawio-diagram.png[] in edit mode:

[source,asciidoc]
----
link:https://www.draw.io/?mode=gitlab#Aueisele-playground%2Fdoc%2Fmaster%2Fdrawio-diagram.png[Edit]
----

The following AsciiDoc snippet creates a link to open link:drawio-diagram.png[] as a new diagram:

[source,asciidoc]
----
link:https://www.draw.io/#Uhttps%3A%2F%2Fgitlab.com%2Fueisele-playground%2Fdoc%2F-%2Fraw%2Fmaster%2Fdrawio-diagram.png[Edit As New]
----
