# Mermaid in Markdown

[[_TOC_]]

## Overview

[Mermaid](https://mermaid-js.github.io/mermaid/#/) enables generation of diagrams from text in a similar manner as markdown.

If you're new to using Mermaid or need help identifying issues in your Mermaid code, the [Mermaid Live Editor](https://mermaid-js.github.io/mermaid-live-editor/) is a helpful tool for creating and resolving issues within Mermaid diagrams.

## Integration

### GitLab

How to use Mermaid with GitLab is described at the GitLab Markdown Documentation in the section [Diagrams and Flowcharts](https://gitlab.com/gitlab-org/gitlab/blob/master/doc/user/markdown.md#diagrams-and-flowcharts).

In order to generate a diagram or flowchart, you should write your text inside the `mermaid` block:

````markdown
```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
````

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

### VSCode

All you need to do is to get the [Mermaid extension](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid) and to enable codes native [Markdown preview feature](https://code.visualstudio.com/docs/languages/markdown) to also parse inline diagrams.

## Diagram Types

### Flowchart

A [flowchart](https://mermaid-js.github.io/mermaid/#/flowchart) is a type of diagram that represents a workflow or process.

The `graph` statement declares a new graph and the direction of the graph layout.

This declares a graph oriented from top to bottom (`TD` or `TB`).

````markdown
```mermaid
graph TD
  Start --> Stop
```
````

```mermaid
graph TD
  Start --> Stop
```

This declares a graph oriented from left to right (`LR`).

````markdown
```mermaid
graph LR
  Start --> Stop
```
````

```mermaid
graph LR
  Start --> Stop
```

It is also possible to declare multiple nodes links in the same line as per below:

````markdown
```mermaid
graph LR
  a --> b & c--> d
```
````

```mermaid
graph LR
  a --> b & c--> d
```

Subgraphs can also be included:

````markdown
```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```
````

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

### Sequence Diagram

A [sequence diagram](https://mermaid-js.github.io/mermaid/#/sequenceDiagram) shows object interactions arranged in time sequence. 

````markdown
```mermaid
sequenceDiagram
  Alice->>John: Hello John, how are you?
  John-->>Alice: Great!
```
````

```mermaid
sequenceDiagram
  Alice->>John: Hello John, how are you?
  John-->>Alice: Great!
```

It is possible to get a sequence number attached to each arrow in a sequence diagram. This can be configured when adding mermaid to the website as shown below:

````markdown
```mermaid
sequenceDiagram
  autonumber
  Alice->>John: Hello John, how are you?
  loop Healthcheck
    John->>John: Fight against hypochondria
  end
  Note right of John: Rational thoughts!
  John-->>Alice: Great!
  John->>Bob: How about you?
  Bob-->>John: Jolly good!
```
````

```mermaid
sequenceDiagram
  autonumber
  Alice->>John: Hello John, how are you?
  loop Healthcheck
    John->>John: Fight against hypochondria
  end
  Note right of John: Rational thoughts!
  John-->>Alice: Great!
  John->>Bob: How about you?
  Bob-->>John: Jolly good!
```

### Class Diagram

A [class diagram](https://mermaid-js.github.io/mermaid/#/classDiagram) is a type of static structure diagram that describes the structure of a system by showing the system's classes, their attributes, operations (or methods), and the relationships among objects. 

````markdown
```mermaid
classDiagram
  Animal <|-- Duck
  Animal <|-- Fish
  Animal <|-- Zebra
  Animal : +int age
  Animal : +String gender
  Animal: +isMammal()
  Animal: +mate()
  class Duck{
    +String beakColor
    +swim()
    +quack()
  }
  class Fish{
    -int sizeInFeet
    -canEat()
  }
  class Zebra{
    +bool is_wild
    +run()
  }
```
````

```mermaid
classDiagram
  Animal <|-- Duck
  Animal <|-- Fish
  Animal <|-- Zebra
  Animal : +int age
  Animal : +String gender
  Animal: +isMammal()
  Animal: +mate()
  class Duck{
    +String beakColor
    +swim()
    +quack()
  }
  class Fish{
    -int sizeInFeet
    -canEat()
  }
  class Zebra{
    +bool is_wild
    +run()
  }
```

### State Diagram

[State diagrams](https://mermaid-js.github.io/mermaid/#/stateDiagram) are used to give an abstract description of the behavior of a system. This behavior is represented as a series of events that can occur in one or more possible states.

The syntax tries to be compliant with the syntax used in PlantUML as this will make it easier for users to share diagrams between Mermaid and PlantUML.

````markdown
```mermaid
stateDiagram
  [*] --> Still
  Still --> [*]

  Still --> Moving
  Moving --> Still
  Moving --> Crash
  Crash --> [*]
```
````

```mermaid
stateDiagram
  [*] --> Still
  Still --> [*]

  Still --> Moving
  Moving --> Still
  Moving --> Crash
  Crash --> [*]
```

### Gantt Diagram

A [Gantt diagram](https://mermaid-js.github.io/mermaid/#/gantt) is a type of bar diagram that illustrates a project schedule.

````markdown
```mermaid
gantt
  title A Gantt Diagram
  dateFormat  YYYY-MM-DD
  section Section
  A task           :a1, 2014-01-01, 30d
  Another task     :after a1  , 20d
  section Another
  Task in sec      :2014-01-12  , 12d
  another task      : 24d
```
````

```mermaid
gantt
  title A Gantt Diagram
  dateFormat  YYYY-MM-DD
  section Section
  A task           :a1, 2014-01-01, 30d
  Another task     :after a1  , 20d
  section Another
  Task in sec      :2014-01-12  , 12d
  another task      : 24d
```

### Pie Chart Diagram

A [pie chart](https://mermaid-js.github.io/mermaid/#/pie) is a circular statistical graphic, which is divided into slices to illustrate numerical proportion.

````markdown
```mermaid
pie title Pets adopted by volunteers
  "Dogs" : 386
  "Cats" : 85
  "Rats" : 15 
```
````

```mermaid
pie title Pets adopted by volunteers
  "Dogs" : 386
  "Cats" : 85
  "Rats" : 15 
```

## Unsupported Diagram Types

### Entity Relationship Diagrams

An [entity relationship diagram](https://mermaid-js.github.io/mermaid/#/entityRelationshipDiagram) describes interrelated things of interest in a specific domain of knowledge.

Officially Mermaid supports entity relationship diagrams. 
Until now, however, GitLab reports an error while rendering the page for the following `mermaid` block:

````markdown
```mermaid
erDiagram
  CUSTOMER ||--o{ ORDER : places
  ORDER ||--|{ LINE-ITEM : contains
  CUSTOMER }|..|{ DELIVERY-ADDRESS : uses 
```
````