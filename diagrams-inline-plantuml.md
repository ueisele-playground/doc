# PlantUML in Markdown

[[_TOC_]]

## Overview

PlantUML is a component that allows to quickly write diagrams using a simple and intuitive textual language.
The textual language in which diagrams can be defined is also called PlantUML (see [PlantUML Language Reference Guide](https://plantuml.com/guide)).

PlantUML also provides an HTTP API to create diagrams from textual descriptions, which is also used by the Markdown PlantUML extension, that is used by GitLab. 

If you're new to using PlantUML or need help identifying issues in your PlantUML code, the [PlantUML Live Editor](http://plantuml.com/plantuml/) is a helpful tool for creating and resolving issues within PlantUML diagrams.

TIP: PlantUML supports not only UML diagram types, but also other [diagram types](https://plantuml.com/en/) like Gantt and Mindmap.

TIP: Besides PlantUML specific textual descriptions, PlantUML supports also [Graphviz (DOT)](https://www.graphviz.org/) and [Ditaa](http://ditaa.sourceforge.net/).

## Integration

### GitLab

How to use PlantUML with GitLab is described at the GitLab PlantUML Integration Documentation in the section [Creating Diagrams](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/administration/integration/plantuml.md#creating-diagrams).

GitLab uses a [PlantUML server](https://github.com/plantuml/plantuml-server) for PlantUML integration (see https://docs.gitlab.com/ee/administration/integration/plantuml.html).

#### Markdown Inline

In order to generate a diagram or flowchart, you should write your text inside the `plantuml` block:

````markdown
```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```
````

The above blocks will be converted to an HTML image tag with source pointing to the GitLab PlantUML instance. This should render a nice diagram instead of the block:

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```

Inside the block you can add any of the supported diagrams by PlantUML such as [Sequence](https://plantuml.com/sequence-diagram), [Use Case](https://plantuml.com/use-case-diagram), [Class](https://plantuml.com/class-diagram), [Activity](https://plantuml.com/activity-diagram-legacy), [Component](https://plantuml.com/component-diagram), [State](https://plantuml.com/state-diagram), and [Object](https://plantuml.com/object-diagram) diagrams. You do not need to use the PlantUML diagram delimiters `@startuml`/`@enduml` as these are replaced by the Markdown `plantuml` block.

Markdown does not support additional attributes like _format_. As output format only `png` is supported.

#### Markdown Include

Markdown itself and also `GitLab Flavored Markdown` does not support file includes like AsciiDoc.

#### PlantUML Include

PlantUML can include files in a diagram with the `!include` directive, using URL. A detailed explanation about the `!include` directive is given in the [PlantUML documentation about preprocessing](https://plantuml.com/de/preprocessing) in the _Include files or URL_ section. 

Like described in [Notes on GitLab Rendering](#-notes-on-gitlab-rendering), the PLantUML `!include` directive should be handled with care, because of image caching behaviour.

Officially, PlantUML suports includes from local files, however, GitLab does not support this.
The reason is, that for rendering, only a single block is transmitted to the PlantUML server. 
In order to support including local files, the include must be handled bevore it is sent to the server. However, this would require, that also the local handler must understand PlantUML language.

PlantUML includes can be used, to load macros or additional stereotypes from remote URLs in order to extend the PlantUML language.

The following block includes a PlantUML file from GitHub in order to load the [C4 PlantUML](#c4-plantuml) extension:

````markdown
```plantuml
!include https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Container.puml
Person(admin, "Administrator")
```
````

```plantuml
!include https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Container.puml
Person(admin, "Administrator")
```

The GitLab PlantUML servers are caching rendered images (see [Notes on GitLab Rendering](#notes-on-gitlab-rendering)). As a result, changes in the included files are not reflected back to the diagrams imediatley. 
Because it is a cache, it could happen at anytime, that a diagram is rendered again. You should also consider, that GitLab runs multiple PlantUML servers, and therefore possibly different versions of the diagram are loaded randomly.
Because of this, you should use `!include` only for macro or stereotype includes like for the `C4-PlantUML` extension. It should not be used if this could affect the meaning of a diagram, like for example an include of a partial diagram.

Includes from GitLab are not possible until now. The PlantUML server cannot open files from GitLab.
For example, `!include https://gitlab.com/ueisele-playground/doc/-/raw/master/plantuml-single-diagram.puml` results in the error `Cannot open URL Server returned HTTP response code: 403`.
I am not sure what the reason exactly is, but it seems that the cause is, that GitLab does not enable CORS for raw file Urls (see [#16732](https://gitlab.com/gitlab-org/gitlab/-/issues/16732)).

As a workaround for GitLab, you could publish the PlantUML files which you would like to include to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) with a [GitLab CI Pipeline](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html).
However, you should consider that files in GitLab Pages are not versioned and that there is only one GitLab Pages site per repository. Pipeline runs in different branches, therefore overwrite the current site. There is an open GitLab Issue with regards to GitLab Pages version support (see [#16208](https://gitlab.com/gitlab-org/gitlab/-/issues/16208)).

#### Notes on GitLab Rendering

GitLab uses a [PlantUML server](https://github.com/plantuml/plantuml-server) for PlantUML integration (see https://docs.gitlab.com/ee/administration/integration/plantuml.html).

`plantuml` blocks are converted to an HTML image tag with source pointing to the GitLab PlantUML instance, which is https://plantuml.gitlab-static.net/. 
The [API of the PlantUML server](https://plantuml.com/de/server) basically accepts the textual diagram description in an [encoded form](https://plantuml.com/de/text-encoding) in the URL.
The PlantUML server answers to such a request with the corresponding rendered image. 

For example, the following block is converted to an HTML image tag with the source pointing to https://plantuml.gitlab-static.net/png/U9npA2v9B2efpStXSifFKj2rKt3CoKnELR1Io4ZDoSdNKIZFI2nHACdCJLN8B5R8B578oo-tvuAe0Ie6wG4gpTG1IG4LgAOabgHgQ78vfEQb0Cq408Ow7OK0.
This URL contains the entire `plantuml` block in an encoded form, which also means, that all blocks with the same content, point to the same URL.

````markdown
```plantuml
Bob -> Alice : hello, what time is it now?
Alice -> Bob : hi, it is %date()
```
````

```plantuml
Bob -> Alice : hello, what time is it now?
Alice -> Bob : hi, it is %date()
```

To demonstrate the consequence of this, we created a `plantuml` block wich contains the function `%date()`. The rendered image therfore contains the timestamp of the point in time, when the diagram was rendered.
You may wonder why the date does not show the current date. The reason for that is, that the GitLab PlantUML server caches created images. 
So basically an image of a textual diagram discription is only rendered once. Which, however, is not completely true. It is rendered once per PlantUML server. It seems that GitLab runs two PlantUML server, because if you refresh your browser (Strg + F5), you may see two different times. Because it is a cache, it could happen at anytime, that a diagram is rendered again.
This is completely fine for static content, however for dynamic content, like the `%date()` function or even worse the `!include` directive this is problematic.

You should use functions (e.g. `%date()`) or directives (e.g. `!include`) which depend on an external state with care! I think those functionality should not be used if this could affect the meaning of a diagram. This, for example, is the case for `%date()` or an include of a partial diagram.
In such cases the content would no longer be based solely on the state of the Git repository itself.

### VSCode

All you need to do is to get the [PlantUML extension](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml) and to enable codes native [Markdown preview feature](https://code.visualstudio.com/docs/languages/markdown) to also parse inline diagrams.

By default the plugin requires a local PlantUML process to be running and accepting the rendering requests. You may switch it to a server for rendering; this could be the official plantuml.com server, an on premise instance or a locally running container. After installing the plugin go to the VS Code options and change the plantuml.render property:

```javascript
// PlantUMLServer: Render diagrams by server which is specified with "plantuml.server". It's much faster, but requires a server.
// Local is the default configuration.
"plantuml.render": "PlantUMLServer",

// Plantuml server to generate UML diagrams on-the-fly.
"plantuml.server": "http://www.plantuml.com/plantuml"
```
It is also possible to run the PlantUML server as a [self-managed server](https://github.com/plantuml/plantuml-server), for example using Docker.

The documentation about the VS Code integration is a rough summary of an existing blog post: https://blog.anoff.io/2018-07-31-diagrams-with-plantuml/

## Diagram Types

### PlantUML

#### Sequence Diagram

A [sequence diagram](https://plantuml.com/en/sequence-diagram) shows object interactions arranged in time sequence. 

````markdown
```plantuml
participant User

User -> A: DoWork
activate A #FFBBBB

A -> A: Internal call
activate A #DarkSalmon

A -> B: << createRequest >>
activate B

B --> A: RequestCreated
deactivate B
deactivate A
A -> User: Done
deactivate A
```
````

```plantuml
participant User

User -> A: DoWork
activate A #FFBBBB

A -> A: Internal call
activate A #DarkSalmon

A -> B: << createRequest >>
activate B

B --> A: RequestCreated
deactivate B
deactivate A
A -> User: Done
deactivate A
```

The keyword `autonumber` is used to automatically add number to messages.

````markdown
```plantuml
autonumber
Bob -> Alice : Authentication Request
Bob <- Alice : Authentication Response
```
````

```plantuml
autonumber
Bob -> Alice : Authentication Request
Bob <- Alice : Authentication Response
```

#### Use Case Diagram

A [use case diagram](https://plantuml.com/en/use-case-diagram) at its simplest is a representation of a user's interaction with the system that shows the relationship between the user and the different use cases in which the user is involved.

````markdown
```plantuml
User -> (Start)
User --> (Use the application) : A small label

:Main Admin: ---> (Use the application) : This is\nyet another\nlabel
```
````

```plantuml
User -> (Start)
User --> (Use the application) : A small label

:Main Admin: ---> (Use the application) : This is\nyet another\nlabel
```

#### Class Diagram

A [class diagram](https://plantuml.com/en/class-diagram) is a type of static structure diagram that describes the structure of a system by showing the system's classes, their attributes, operations (or methods), and the relationships among objects. 

````markdown
```plantuml
package "Classic Collections" #DDDDDD {
  Object <|-- ArrayList
}

package net.sourceforge.plantuml {
  Object <|-- Demo1
  Demo1 *- Demo2
}
```
````

```plantuml
package "Classic Collections" #DDDDDD {
  Object <|-- ArrayList
}

package net.sourceforge.plantuml {
  Object <|-- Demo1
  Demo1 *- Demo2
}
```

#### Activity Diagram

[Activity diagrams](https://plantuml.com/en/activity-diagram-beta) are graphical representations of workflows of stepwise activities and actions with support for choice, iteration and concurrency. 

````markdown
```plantuml
start

if (Graphviz installed?) then (yes)
  :process all\ndiagrams;
else (no)
  :process only
  __sequence__ and __activity__ diagrams;
endif

stop
```
````

```plantuml
start

if (Graphviz installed?) then (yes)
  :process all\ndiagrams;
else (no)
  :process only
  __sequence__ and __activity__ diagrams;
endif

stop
```

#### Component Diagram

A [component diagram](https://plantuml.com/en/component-diagram) depicts how components are wired together to form larger components or software systems.

````markdown
```plantuml
package "Some Group" {
  HTTP - [First Component]
  [Another Component]
}

node "Other Groups" {
  FTP - [Second Component]
  [First Component] --> FTP
}

database "MySql" {
  folder "This is my folder" {
    [Folder 3]
  }
  frame "Foo" {
    [Frame 4]
  }
}

[Another Component] --> [Folder 3]
[Folder 3] --> [Frame 4]
```
````

```plantuml
package "Some Group" {
  HTTP - [First Component]
  [Another Component]
}

node "Other Groups" {
  FTP - [Second Component]
  [First Component] --> FTP
}

database "MySql" {
  folder "This is my folder" {
    [Folder 3]
  }
  frame "Foo" {
    [Frame 4]
  }
}

[Another Component] --> [Folder 3]
[Folder 3] --> [Frame 4]
```

#### State Diagram

[State diagrams](https://plantuml.com/en/state-diagram) are used to give an abstract description of the behavior of a system. This behavior is represented as a series of events that can occur in one or more possible states. 

````markdown
```plantuml
[*] --> State1
State1 --> [*]
State1 : this is a string
State1 : this is another string

State1 -> State2
State2 --> [*]
```
````

```plantuml
[*] --> State1
State1 --> [*]
State1 : this is a string
State1 : this is another string

State1 -> State2
State2 --> [*]
```

#### Object Diagram

An [object diagram](https://plantuml.com/en/object-diagram) shows a complete or partial view of the structure of a modeled system at a specific time. 

````markdown
```plantuml
object Object01
object Object02
object Object03
object Object04
object Object05
object Object06
object Object07
object Object08

Object01 <|-- Object02
Object03 *-- Object04
Object05 o-- "4" Object06
Object07 .. Object08 : some labels
```
````

```plantuml
object Object01
object Object02
object Object03
object Object04
object Object05
object Object06
object Object07
object Object08

Object01 <|-- Object02
Object03 *-- Object04
Object05 o-- "4" Object06
Object07 .. Object08 : some labels
```

#### Deployment Diagram

A [deployment diagram](https://plantuml.com/en/deployment-diagram) models the physical deployment of artifacts on nodes.

````markdown
```plantuml
node node1
node node2
node node3
node node4
node node5
node1 -- node2 : label1
node1 .. node3 : label2
node1 ~~ node4 : label3
node1 == node5
```
````

```plantuml
node node1
node node2
node node3
node node4
node node5
node1 -- node2 : label1
node1 .. node3 : label2
node1 ~~ node4 : label3
node1 == node5
```

#### Timing Diagram

A [timing diagram](https://plantuml.com/en/timing-diagram) is a specific type of interaction diagram, where the focus is on timing constraints. 

````markdown
```plantuml
robust "Web Browser" as WB
concise "Web User" as WU

@0
WU is Idle
WB is Idle

@100
WU is Waiting
WB is Processing

@300
WB is Waiting
```
````

```plantuml
robust "Web Browser" as WB
concise "Web User" as WU

@0
WU is Idle
WB is Idle

@100
WU is Waiting
WB is Processing

@300
WB is Waiting
```

#### Wireframe (Salt)

[Salt](https://plantuml.com/en/salt) is a subproject included in PlantUML that may help you to design graphical interface. 

TIP: A Salt diagram must be started with the `salt` keyword.

````markdown
```plantuml
salt
{
  Just plain text
  [This is my button]
  ()  Unchecked radio
  (X) Checked radio
  []  Unchecked box
  [X] Checked box
  "Enter text here   "
  ^This is a droplist^
}
```
````

```plantuml
salt
{
  Just plain text
  [This is my button]
  ()  Unchecked radio
  (X) Checked radio
  []  Unchecked box
  [X] Checked box
  "Enter text here   "
  ^This is a droplist^
}
```

#### Archimate Diagram

[ArchiMate](http://pubs.opengroup.org/architecture/archimate3-doc/) is an open and independent enterprise architecture modeling language to support the description, analysis and visualization of architecture within and across business domains in an unambiguous way.
PlantUML provides a textual language for describing [archimate diagrams](https://plantuml.com/en/archimate-diagram).

[Archimate-PlantUML](https://github.com/ebbypeter/Archimate-PlantUML) defines marcros which simplify the creation of ArchiMate diagrams.
Because this is an extension to the language, it also also described in the [Extended Diagram Types](#extended-diagram-types) section at [Archimate PlantUML](#archimate-plantuml).

````markdown
```plantuml
!define Junction_Or circle #black
!define Junction_And circle #whitesmoke

Junction_And JunctionAnd
Junction_Or JunctionOr

archimate #Technology "VPN Server" as vpnServerA <<technology-device>>

rectangle GO #lightgreen
rectangle STOP #red
rectangle WAIT #orange
GO -up-> JunctionOr
STOP -up-> JunctionOr
STOP -down-> JunctionAnd
WAIT -down-> JunctionAnd
```
````

```plantuml
!define Junction_Or circle #black
!define Junction_And circle #whitesmoke

Junction_And JunctionAnd
Junction_Or JunctionOr

archimate #Technology "VPN Server" as vpnServerA <<technology-device>>

rectangle GO #lightgreen
rectangle STOP #red
rectangle WAIT #orange
GO -up-> JunctionOr
STOP -up-> JunctionOr
STOP -down-> JunctionAnd
WAIT -down-> JunctionAnd
```

#### Gantt Diagram

A [Gantt diagram](https://plantuml.com/en/gantt-diagram) is a type of bar diagram that illustrates a project schedule.

The Gantt is described in natural language, using very simple sentences (subject-verb-complement). 

````markdown
```plantuml
Project starts the 13th of april 2020
[Prototype design] lasts 8 days
[Prototype design] is colored in Fuchsia/FireBrick
[Prototype design] is 40% completed
[Test prototype] starts at [Prototype design]'s end
[Test prototype] lasts 15 days
[Test prototype] is 0% completed
```
````

```plantuml
Project starts the 13th of april 2020
[Prototype design] lasts 8 days
[Prototype design] is colored in Fuchsia/FireBrick
[Prototype design] is 40% completed
[Test prototype] starts at [Prototype design]'s end
[Test prototype] lasts 15 days
[Test prototype] is 0% completed
```

#### MindMap Diagram

A [mind map](https://plantuml.com/en/mindmap-diagram) is a diagram used to visually organize information. A mind map is hierarchical and shows relationships among pieces of the whole.

TIP: A MindMap diagram must be started with the `@startmindmap` keyword and completed with the `@endmindmap` keyword.

````markdown
```plantuml
@startmindmap
* root node
** some first level node
***_ second level node
***_ another second level node
***_ foo
***_ bar
***_ foobar
** another first level node
@endmindmap
```
````

```plantuml
@startmindmap
* root node
** some first level node
***_ second level node
***_ another second level node
***_ foo
***_ bar
***_ foobar
** another first level node
@endmindmap
```

#### Work Breakdown Structure (WBS) Diagram

A [work-breakdown structure (WBS)](https://plantuml.com/en/wbs-diagram) in project management and systems engineering, is a deliverable-oriented breakdown of a project into smaller components.

TIP: A WBS diagram must be started with the `@startwbs` keyword and completed with the `@endwbs` keyword.

````markdown
```plantuml
@startwbs
* Business Process Modelling WBS
** Launch the project
*** Complete Stakeholder Research
*** Initial Implementation Plan
** Design phase
*** Model of AsIs Processes Completed
**** Model of AsIs Processes Completed1
**** Model of AsIs Processes Completed2
*** Measure AsIs performance metrics
*** Identify Quick Wins
** Complete innovate phase
@endwbs
```
````

```plantuml
@startwbs
* Business Process Modelling WBS
** Launch the project
*** Complete Stakeholder Research
*** Initial Implementation Plan
** Design phase
*** Model of AsIs Processes Completed
**** Model of AsIs Processes Completed1
**** Model of AsIs Processes Completed2
*** Measure AsIs performance metrics
*** Identify Quick Wins
** Complete innovate phase
@endwbs
```

#### Entity Relationship Diagram

An [entity relationship diagram](https://plantuml.com/en/ie-diagram) describes interrelated things of interest in a specific domain of knowledge.

This is an extension to the existing [Class Diagram](#class-diagram).

````markdown
```plantuml
hide circle
skinparam linetype ortho

entity "Entity01" as e01 {
  *e1_id : number <<generated>>
  --
  *name : text
  description : text
}

entity "Entity02" as e02 {
  *e2_id : number <<generated>>
  --
  *e1_id : number <<FK>>
  other_details : text
}

entity "Entity03" as e03 {
  *e3_id : number <<generated>>
  --
  e1_id : number <<FK>>
  other_details : text
}

e01 ||..o{ e02
e01 |o..o{ e03
```
````

```plantuml
hide circle
skinparam linetype ortho

entity "Entity01" as e01 {
  *e1_id : number <<generated>>
  --
  *name : text
  description : text
}

entity "Entity02" as e02 {
  *e2_id : number <<generated>>
  --
  *e1_id : number <<FK>>
  other_details : text
}

entity "Entity03" as e03 {
  *e3_id : number <<generated>>
  --
  e1_id : number <<FK>>
  other_details : text
}

e01 ||..o{ e02
e01 |o..o{ e03
```

### Ditaa

[Ditaa](http://ditaa.sourceforge.net/) interprets ascci art as a series of open and closed shapes, but it also uses special markup syntax to increase the possibilities of shapes and symbols that can be rendered.

TIP: A [Ditaa diagram](https://plantuml.com/en/ditaa) must be started with the `ditaa` keyword.

````markdown
```plantuml
ditaa
+--------+   +-------+    +-------+
|        +---+ ditaa +--> |       |
|  Text  |   +-------+    |diagram|
|Document|   |!magic!|    |       |
|     {d}|   |       |    |       |
+---+----+   +-------+    +-------+
    :                         ^
    |       Lots of work      |
    +-------------------------+
```
````

```plantuml
ditaa
+--------+   +-------+    +-------+
|        +---+ ditaa +--> |       |
|  Text  |   +-------+    |diagram|
|Document|   |!magic!|    |       |
|     {d}|   |       |    |       |
+---+----+   +-------+    +-------+
    :                         ^
    |       Lots of work      |
    +-------------------------+
```

### DOT (Graphviz)

[Graphviz](https://www.graphviz.org/) is open source graph visualization software. Graph visualization is a way of representing structural information as diagrams of abstract graphs and networks.
The graph description languange which is used by Graphviz is [dot](https://graphviz.gitlab.io/_pages/doc/info/lang.html).

Since [PlantUML uses DOT/Graphviz](https://plantuml.com/en/dot), it is possible to directly use DOT language.

````markdown
```plantuml
digraph G {
  subgraph cluster_0 {
    style=filled;
    color=lightgrey;
    node [style=filled,color=white];
    a0 -> a1 -> a2;
    label = "process #1";
  }
  subgraph cluster_1 {
    node [style=filled];
    b0 -> b1 -> b2;
    label = "process #2";
    color=blue
  }
  start -> a0;
  start -> b0;
  a1 -> b2;
  b1 -> a2;
  a2 -> a0;
  a2 -> end;
  b2 -> end;
  start [shape=Mdiamond];
  end [shape=Msquare];
}
```
````

```plantuml
digraph G {
  subgraph cluster_0 {
    style=filled;
    color=lightgrey;
    node [style=filled,color=white];
    a0 -> a1 -> a2;
    label = "process #1";
  }
  subgraph cluster_1 {
    node [style=filled];
    b0 -> b1 -> b2;
    label = "process #2";
    color=blue
  }
  start -> a0;
  start -> b0;
  a1 -> b2;
  b1 -> a2;
  a2 -> a0;
  a2 -> end;
  b2 -> end;
  start [shape=Mdiamond];
  end [shape=Msquare];
}
```

#### Undirected Graphs

At its simplest, `dot` can be used to describe an `undirected graph`.

````markdown
```plantuml
graph graphname {
  a -- b -- c;
  b -- d;
}
```
````

```plantuml
graph graphname {
  a -- b -- c;
  b -- d;
}
```

#### Directed Graphs

Similar to undirected graphs, `dot` can describe `directed graphs`, such as flowcharts and dependency trees.

````markdown
```plantuml
digraph graphname {
  a -> b -> c;
  b -> d;
}
```
````

```plantuml
digraph graphname {
  a -> b -> c;
  b -> d;
}
```

#### Subgraphs

A graph can also have multiple `subgraphs`.

````markdown
```plantuml
digraph G {
  A; B; C
  subgraph Rel1 {
    edge [dir=none, color=red]
    A -> B -> C -> A
  }
  subgraph Rel2 {
    edge [color=blue]
    B -> C
    C -> A
  }
}
```
````

```plantuml
digraph G {
  A; B; C
  subgraph Rel1 {
    edge [dir=none, color=red]
    A -> B -> C -> A
  }
  subgraph Rel2 {
    edge [color=blue]
    B -> C
    C -> A
  }
}
```

## Extended Diagram Types

Diagram types that are not directly supported by PlantUML, but made available through macros and additional steriotypes.

### C4 PlantUML

The [C4 model](https://c4model.com/#coreDiagrams) for software architecture is an "abstraction-first" approach to diagramming, based upon abstractions that reflect how software architects and developers think about and build software. C4 stands for context, containers, components, and code — a set of hierarchical diagrams that you can use to describe your software architecture at different zoom levels, each useful for different audiences.

[C4-PlantUML](https://github.com/RicardoNiepel/C4-PlantUML) combines the benefits of PlantUML and the C4 model for providing a simple way of describing and communicate software architectures.

C4-PlantUML includes macros, stereotypes, and other goodies for creating C4 diagrams with PlantUML.

At the top of your `plantuml` block, you need to include the [C4_Context.puml](https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Context.puml), [C4_Container.puml](https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Container.puml) or [C4_Component.puml](https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Component.puml) file found in the root of this repo.

```
!include https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Container.puml
```

For example a C4 Container diagram may be defined like this:

````markdown
```plantuml
!include https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Container.puml

Person(admin, "Administrator")
System_Boundary(c1, "Sample System") {
  Container(web_app, "Web Application", "C#, ASP.NET Core 2.1 MVC", "Allows users to compare multiple Twitter timelines")
}
System(twitter, "Twitter")

Rel(admin, web_app, "Uses", "HTTPS")
Rel(web_app, twitter, "Gets tweets from", "HTTPS")
```
````

```plantuml
!include https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Container.puml

Person(admin, "Administrator")
System_Boundary(c1, "Sample System") {
  Container(web_app, "Web Application", "C#, ASP.NET Core 2.1 MVC", "Allows users to compare multiple Twitter timelines")
}
System(twitter, "Twitter")

Rel(admin, web_app, "Uses", "HTTPS")
Rel(web_app, twitter, "Gets tweets from", "HTTPS")
```

The [supported diagram types](https://github.com/RicardoNiepel/C4-PlantUML#supported-diagram-types) are documented on the [C4-PlantUML](https://github.com/RicardoNiepel/C4-PlantUML#supported-diagram-types) GitHub page.

### Archimate PlantUML

[ArchiMate](http://pubs.opengroup.org/architecture/archimate3-doc/) is an open and independent enterprise architecture modeling language to support the description, analysis and visualization of architecture within and across business domains in an unambiguous way.

PlantUML provides a textual language for describing [archimate diagrams](https://plantuml.com/en/archimate-diagram) as described at [Archimate Diagram](#archimate-diagram).

[Archimate-PlantUML](https://github.com/ebbypeter/Archimate-PlantUML) combines the benefits of PlantUML and ArchiMate for providing a simple way of creating and managing ArchiMate diagrams. The Archimate-PlantUML is a set of macros and other includes written on top of [PlantUML Archimate specification](http://plantuml.com/archimate-diagram), with an aim to simplify the syntax for creating elements and defining relationships.

At the top of your `plantuml` block, you need to include the [Archimate.puml](https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml) file.

```
!include https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/master/Archimate.puml
```

For example a Archimate PlantUML diagram may be defined like this:

````markdown
```plantuml
!include https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/v1.0.0/Archimate.puml

title Archimate Sample - Requirement & Application Services

Motivation_Requirement(ReqPayrollStandard, "Do Payroll with a standard system")
Motivation_Requirement(ReqBudgetPlanning, "Do budget planning within the ERP system")

Application_Service(ASPayroll,"Payroll Service")
Application_Service(ASBudgetPlanning,"Budget Planning Service")
Application_Component(ACSAPFinanceAccRec, "SAP Finance - Accounts Recievables")
Application_Component(ACSAPHR, "SAP Human Resources")
Application_Component(ACSAPFin, "SAP Finance")
Application_Component(ACSAP,"SAP") 

Rel_Realization_Up(ASPayroll, ReqPayrollStandard)
Rel_Realization_Up(ASBudgetPlanning, ReqBudgetPlanning)
Rel_Realization_Up(ACSAPFinanceAccRec, ASBudgetPlanning)
Rel_Realization_Up(ACSAPHR, ASPayroll)

Rel_Composition_Up(ACSAPFin, ACSAPFinanceAccRec)
Rel_Composition_Up(ACSAP, ACSAPHR)
Rel_Composition_Up(ACSAP, ACSAPFin)
```
````

```plantuml
!include https://raw.githubusercontent.com/ebbypeter/Archimate-PlantUML/v1.0.0/Archimate.puml

title Archimate Sample - Requirement & Application Services

Motivation_Requirement(ReqPayrollStandard, "Do Payroll with a standard system")
Motivation_Requirement(ReqBudgetPlanning, "Do budget planning within the ERP system")

Application_Service(ASPayroll,"Payroll Service")
Application_Service(ASBudgetPlanning,"Budget Planning Service")
Application_Component(ACSAPFinanceAccRec, "SAP Finance - Accounts Recievables")
Application_Component(ACSAPHR, "SAP Human Resources")
Application_Component(ACSAPFin, "SAP Finance")
Application_Component(ACSAP,"SAP") 

Rel_Realization_Up(ASPayroll, ReqPayrollStandard)
Rel_Realization_Up(ASBudgetPlanning, ReqBudgetPlanning)
Rel_Realization_Up(ACSAPFinanceAccRec, ASBudgetPlanning)
Rel_Realization_Up(ACSAPHR, ASPayroll)

Rel_Composition_Up(ACSAPFin, ACSAPFinanceAccRec)
Rel_Composition_Up(ACSAP, ACSAPHR)
Rel_Composition_Up(ACSAP, ACSAPFin)
```

The [usage](https://github.com/ebbypeter/Archimate-PlantUML#usage) is explained on the [Archimate-PlantUML](https://github.com/ebbypeter/Archimate-PlantUML#usage) GitHub page.

## Unsupported Diagram Types

### AsciiMath and JLaTeXMath

The [PlantUML documentation](https://plantuml.com/en/ascii-math) states that math formulars can be expressed with [AsciiMath](http://asciimath.org/) or [JLaTeXMath](https://github.com/opencollab/jlatexmath) syntax.
However, GitLab does not support this PlantUML feature, and the following `plantuml` blocks therefore are rendered as plain text. 

````markdown
```plantuml
@startmath
f(t)=(a_0)/2 + sum_(n=1)^ooa_ncos((npit)/L)+sum_(n=1)^oo b_n\ sin((npit)/L)
@endmath
```
````

```plantuml
@startmath
f(t)=(a_0)/2 + sum_(n=1)^ooa_ncos((npit)/L)+sum_(n=1)^oo b_n\ sin((npit)/L)
@endmath
```

````markdown
```plantuml
@startlatex
\sum_{i=0}^{n-1} (a_i + b_i^2)
@endlatex
```
````

```plantuml
@startlatex
\sum_{i=0}^{n-1} (a_i + b_i^2)
@endlatex
```